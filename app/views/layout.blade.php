<DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8" />
		<title>
			@section('title')
            Mezuniyet Yıllıgı
			@show
        </title>
		<meta name="keywords" content="yıllık,omü yıllık, mezuniyet yıllıgı" />
		<meta name="author" content="mekomu" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Bootstrap -->
		<link rel="stylesheet" href="{{{ asset('assets/css/bootstrap.css') }}}">
		<link rel="stylesheet" href="{{{ asset('assets/css/bootstrap.min.css') }}}">
	</head>
	<body>
		
		@yield('content')
	
	<script type="text/javascript" src="{{{ asset('assets/js/bootstrap.js') }}}"></script>
	<script type="text/javascript" src="{{{ asset('assets/js/bootstrap.min.js') }}}"></script>
	<script src="https://code.jquery.com/jquery.js"></script>
	</body>
</html>
