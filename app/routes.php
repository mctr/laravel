<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('kullanicilar', function(){ return View::make('kullanicilar'); });

Route::get('/', function(){	return View::make('hello'); });

Route::get('Users', function()
{
    $users = User::all(); //Users tablosundaki tüm verileri $users değişkenine atar

    return View::make('Users')->with('users', $users);
});
Route::any('bir/sey', function() // any -----> Tüm metodlar(GET, POST) için kullanılır
{
	return 'Merhabalar Dostum!';
});
